package Pribehy;

import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Rectangle;


import main.NesnupejteDrozdi;
import main.Okno;
import main.RelatvniVelikost;

@SuppressWarnings("serial")
public class Pribeh extends JPanel {
	JLabel textLabel = new JLabel();
	Okno okno;
	JButton zpet = new JButton();
	JButton dal = new JButton();

	// zakladni 
	final String quit =Okno.ziskatCestu("rsc/cedulePribehy/quit.png");
	//final String quit2 ="/rsc/cedulePribehy/quit.png";
	final String sipka = Okno.ziskatCestu("rsc/cedulePribehy/sipka.png");

	public Pribeh(Okno okno,String text) {
		this.okno = okno;
		
		//okno.smazat();
		okno.defOkno();

		this.zaklad(text);
	}

	void zaklad(String text) {
		okno.hlPanel.setLayout(null);
		okno.hlPanel.setBounds(RelatvniVelikost.obdelnik(0,0,100,75));
		
		okno.odpovediPanel.setLayout(null);
		okno.odpovediPanel.setBounds(RelatvniVelikost.obdelnik(0,75,100,25));

		textLabel.setFont(new Font("Consolas", Font.PLAIN, 35));
		textLabel.setForeground(Color.white);
		textLabel.setBounds(0, 0, okno.hlPanel.getWidth(), okno.hlPanel.getHeight());
		textLabel.setBackground(Color.black);
		textLabel.setOpaque(false);
		textLabel.setHorizontalAlignment(getWidth() / 2);

		okno.hlPanel.add(textLabel);
		this.nastavBarvy(new Color(80, 80, 80), new Color(36, 137, 176));
		this.nastavSpodniListu("zpět", "dále");
		
		this.nastavPribeh(text);
		// zakladni nastaveni
				if (textLabel.getText() == null)
					this.nastavPribeh("Ticho jako po pěšině...");
	}

	public void nastavPribeh(String textPribeh) {
		textLabel.setText(textPribeh);
	}

	public void nastavBarvy(Color barva1, Color barva2) {
		okno.hlPanel.setBackground(barva1);
		okno.odpovediPanel.setBackground(barva2);
	}

	public void nastavSpodniListu(String string, String string2) {
		
		zpet.setBounds(new Rectangle(RelatvniVelikost.procentoX(2), RelatvniVelikost.procentoY(10,okno.odpovediPanel.getHeight()), RelatvniVelikost.procentoX(10), RelatvniVelikost.procentoY(80,okno.odpovediPanel.getHeight())));
		zpet.setIcon(Okno.resizeImage(new ImageIcon(quit),zpet.getWidth(), zpet.getHeight()));
		zpet.setBorder(null);
		zpet.setOpaque(false);
		zpet.addActionListener(e -> {
			synchronized (this) {
				NesnupejteDrozdi.jitdal = false;
				notify();
			}
		});
		okno.odpovediPanel.add(zpet);
		
		dal.setBounds(new Rectangle(RelatvniVelikost.procentoX(88), RelatvniVelikost.procentoY(10,okno.odpovediPanel.getHeight()), RelatvniVelikost.procentoX(10), RelatvniVelikost.procentoY(80,okno.odpovediPanel.getHeight())));
		dal.setIcon(Okno.resizeImage(new ImageIcon(sipka),dal.getWidth(), dal.getHeight()));
		dal.setOpaque(false);
		dal.setContentAreaFilled(false);
		dal.setBorder(null);
		dal.addActionListener(e -> {
			synchronized (this) {
				NesnupejteDrozdi.jitdal = true;
				notify();
			}
		});
		okno.odpovediPanel.add(dal);
		okno.odpovediPanel.repaint();
	}

	public void cekatNaVstup() {
		
		synchronized (this) {
			try {
				this.wait();
			} catch (InterruptedException ex) {
				System.out.println("CHYBA -- Pribeh  " + ex);
			}
		}
		okno.hlPanel.remove(textLabel);
		okno.odpovediPanel.remove(zpet);
		okno.odpovediPanel.remove(dal);
		System.out.println("KONEC -- Pribeh   " + Thread.currentThread());
		//System.out.println(" "+ Thread.getAllStackTraces());
	}
}

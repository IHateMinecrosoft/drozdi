package main;

import javax.swing.JFrame;

import Levely.Level_0.Level0;
import Levely.Level_1.Level1;
import Levely.Level_2.Level2;
import Levely.Level_3.Level3;
import PribehyLevely.Pribeh1;
import PribehyLevely.Pribeh2;
import PribehyLevely.Pribeh3;
//test
public class NesnupejteDrozdi {
	public static int postup, level3Level;
	public static boolean jitdal;
	public static long casLevel1= 2147483647, casLevel2= 2147483647;
	public static long[] mapaCasy= {2147483647, 2147483647, 2147483647, 2147483647, 2147483647, 2147483647};
	public static String ucet = "NesnupejteDrozdi";
	public static String ucetCesta = "src/ucty/" + NesnupejteDrozdi.ucet+".json";
	// public int
	public static void main(String[] args) {
		Okno okno = new Okno(500, 300);
		okno.setExtendedState(JFrame.MAXIMIZED_BOTH);
		okno.setVisible(true);
		RelatvniVelikost.setMaximum(okno.getSize().width, okno.getSize().height);
		postup = 0;
		okno.setTitle("Šňupejte droždí");
		do {
			
			switch (postup) {
			case 0: {System.out.println("Level0");
				new Level0(okno);
				break;
			}
			case 1: {
				new Pribeh1(okno);
				if (jitdal == true) {
					new Level1(okno);
					postup++;
				} else {
					postup = 0;
				}
				break;
			}
			case 2: {
				new Pribeh2(okno);
				if (jitdal == true) {
					new Level2(okno);
					postup++;
				} else {
					postup = 0;;
				}
				break;
			}
			case 3: {
				new Pribeh3(okno);
				if (jitdal == true) {
					new Level3(okno);
					postup = 0;
				} else {
					postup = 0;
				}
				break;
			}
			}
		} while (0 <= postup && postup < 4);
		
		System.out.println("KONEC Main");
		System.exit(0);
	}
}

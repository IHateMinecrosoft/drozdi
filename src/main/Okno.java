package main;

import java.awt.*;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.io.File;



@SuppressWarnings("serial")
public class Okno extends JFrame {
	public JPanel hlPanel;
	public JPanel otazkyPanel, odpovediPanel;
	public JLabel otazkyLabel;
	public JTextField textField;
	public int vyskaOkna;
	public int sirkaOkna;
	
	ImageIcon image = new ImageIcon("rsc/drozdi.png");
	public static Thread t;
	public Okno(int sirka, int vyska) {
		setUndecorated(true); // zniči okno
		t = Thread.currentThread();  
		vyskaOkna = vyska;
		sirkaOkna = sirka;
		// tohle se nebude m2nit
		this.setTitle("Nešňupejte droždí ");
		this.setResizable(false);
		this.setSize(sirkaOkna, vyskaOkna);
		this.setLayout(null);

		this.setIconImage(image.getImage());
		this.getContentPane().setBackground(new Color(125, 156, 158));
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		defOkno();
	}
	public void smazat() {
		this.getContentPane().removeAll();
	}
	public void defOkno() {

		otazkyLabel = new JLabel();
		hlPanel = new JPanel();
		otazkyPanel = new JPanel();
		odpovediPanel = new JPanel();
		
		
		otazkyPanel.add(otazkyLabel); 
		this.add(hlPanel);
		this.add(otazkyPanel);
		this.add(odpovediPanel);

	}

	// Funkce na zmeneni obrazku (puvodni) na danou velikost
	public static ImageIcon resizeImage(ImageIcon puvodni, int x, int y) {

		Image img = puvodni.getImage();
		Image imgScale = img.getScaledInstance(x, y, Image.SCALE_SMOOTH);
		ImageIcon scaledIcon = new ImageIcon(imgScale);
		return scaledIcon;
	}

	public void nastavOtazku(Okno okno, JPanel panel, JLabel label,JButton tlacitko1,JButton tlacitko2, String otazka, String odpoved1, String odpoved2) {

		label.setText(otazka);

		tlacitko1.setLayout(null);
		//tlacitko1.setBounds(okno.sirkaOkna * 2 / 24, 20, okno.sirkaOkna * 9 / 24, 100);
		tlacitko1.setBounds(RelatvniVelikost.obdelnik(15,10, 30, 80, RelatvniVelikost.getMaximum().x,RelatvniVelikost.procentoY(15,RelatvniVelikost.getMaximum().y)));
		tlacitko1.setFont(new Font("Consolas", Font.PLAIN, 25));
		tlacitko1.setText(odpoved1);
		tlacitko1.setFocusable(false);
		tlacitko1.setForeground(new Color(250, 250, 250));
		tlacitko1.setBackground(new Color(50, 50, 50));
		// tlacitko1.pressedBackgroundColor =Color.PINK;
		tlacitko1.setBorder(null);
		panel.add(tlacitko1);

		tlacitko2.setLayout(null);
		//tlacitko2.setBounds(okno.sirkaOkna * 12 / 24, 20, okno.sirkaOkna * 9 / 24, 100);
		tlacitko2.setBounds(RelatvniVelikost.obdelnik(55,10, 30, 80, RelatvniVelikost.getMaximum().x,RelatvniVelikost.procentoY(15,RelatvniVelikost.getMaximum().y)));
		tlacitko2.setFont(new Font("Consolas", Font.PLAIN, 25));
		tlacitko2.setText(odpoved2);
		tlacitko2.setFocusable(false);
		tlacitko2.setForeground(new Color(250, 250, 250));
		tlacitko2.setBackground(new Color(50, 50, 50));
		tlacitko2.setBorder(null);
		panel.add(tlacitko2);

		// return 0;
	}
	@Override
	  public synchronized void setExtendedState(final int state) {
	    if ((state & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH) {
	      final GraphicsConfiguration cfg = getGraphicsConfiguration();
	      final Insets screenInsets = getToolkit().getScreenInsets(cfg);
	      final Rectangle screenBounds = cfg.getBounds();
	      final int x = screenInsets.left + screenBounds.x * 0;
	      final int y = screenInsets.top + screenBounds.y * 0;
	      final int w = screenBounds.width - screenInsets.right - screenInsets.left;
	      final int h = screenBounds.height - screenInsets.bottom - screenInsets.top;
	      final Rectangle maximizedBounds = new Rectangle(x, y, w, h);

	      //System.out.println("cfg (" + cfg + ") screen.{bounds: " + screenBounds + ", insets: " + screenInsets + ", maxBounds: " + maximizedBounds);

	      super.setMaximizedBounds(maximizedBounds);
	    }
	    super.setExtendedState(state);
	  }
	public static String ziskatCestu(String cesta) {
		return new File(cesta).getAbsolutePath().replace("\\", "/");
	}
}

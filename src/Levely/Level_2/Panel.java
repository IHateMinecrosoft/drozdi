package Levely.Level_2;

import java.awt.*;

import main.*;

import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.util.ArrayList;
import java.util.Random;

@SuppressWarnings("serial")
public class Panel extends JPanel implements Runnable {
	Vozik vozik;
	Thread hraThread;
	Okno mistniOkno;
	ImageIcon vozikimg;
	ArrayList<Jidlo> pult = new ArrayList<>();
	Random random = new Random();
	// Rectangle f = new Rectangle();
	// long stopky = 2 * 60 * 60;
	long stopky = 2 *60*60;
	// nacteni obrazku
	public static ImageIcon drozdi;
	public static ImageIcon kecup;
	public static ImageIcon clock;
	public Pohyb keyK;
	int pocetDrozdi = 0, pocetKecup = 0;
	int drozdiMax = 20;
	JLabel pocitadloDrozdi, pocitadloKecup, pocitadloStopky;
	JButton startTlacitko = new JButton();
	int konecDrozdi = 0; // pro konecne padajici drozdi
	Level2 level2;
	public Panel(Okno okno, Level2 level2) {
		this.level2 = level2;
		// nastaveni okna
		//this.setBounds(0, 0, okno.sirkaOkna - 15, okno.vyskaOkna - 150); // nevim proc je tam 15px mimo obraz
		this.setBounds(RelatvniVelikost.obdelnik(0, 0 ,100, 85));
		this.setDoubleBuffered(true);

		mistniOkno = okno;
		//nastaveni obrazku;
		Jidlo.velikost = RelatvniVelikost.getMaximum().x/40;
		drozdi = Okno.resizeImage(new ImageIcon("rsc/drozdi.png"), Jidlo.velikost, Jidlo.velikost);
		kecup = Okno.resizeImage(new ImageIcon("rsc/Level2/kecup.png"), Jidlo.velikost, Jidlo.velikost);
		clock = Okno.resizeImage(new ImageIcon("rsc/Level2/time.png"), Jidlo.velikost, Jidlo.velikost);
		
		// nastaveni voziku
		Vozik vozik = new Vozik(this.getWidth(), this.getHeight());
		this.vozik = vozik;

		keyK = new Pohyb(vozik, this);
		this.addKeyListener(keyK);
		
		this.setFocusable(true);
		vozikimg = vozik.imgR;

		//okno.otazkyPanel.setBounds(0, okno.vyskaOkna - 150, okno.sirkaOkna, 50);
		// okno.otazkyPanel.getHeight());
		okno.otazkyPanel.setBounds(RelatvniVelikost.obdelnik(0,85,100,5));
		okno.otazkyLabel.setVerticalTextPosition(JLabel.CENTER);
		mistniOkno.otazkyLabel.setFont(new Font("Consolas", Font.PLAIN, 25));
		mistniOkno.otazkyLabel.setText("Nekupuj to droždí! Musíš odolat! ");

		okno.odpovediPanel.setBounds(RelatvniVelikost.obdelnik(0,90,100,10));
		okno.odpovediPanel.setBackground(Color.black);

		JLabel pocitadloDrozdi = new JLabel();
		pocitadloDrozdi.setLayout(null);
		pocitadloDrozdi.setBounds(0, 0, okno.sirkaOkna * 1 / 8, 65);
		pocitadloDrozdi.setBounds(RelatvniVelikost.obdelnik(0, 0, 10, 50, okno.odpovediPanel.getWidth(), okno.odpovediPanel.getHeight()));
		pocitadloDrozdi.setFont(new Font("Consolas", Font.PLAIN, 25));
		pocitadloDrozdi.setForeground(Color.white);
		pocitadloDrozdi.setHorizontalAlignment(JLabel.CENTER);
		pocitadloDrozdi.setIcon(drozdi);
		okno.odpovediPanel.add(pocitadloDrozdi);

		this.pocitadloDrozdi = pocitadloDrozdi;

		JLabel pocitadloKecup = new JLabel();
		pocitadloKecup.setLayout(null);
		pocitadloKecup.setBounds(RelatvniVelikost.obdelnik(10, 0, 10, 50, okno.odpovediPanel.getWidth(), okno.odpovediPanel.getHeight()));
		pocitadloKecup.setFont(new Font("Consolas", Font.PLAIN, 25));
		pocitadloKecup.setForeground(Color.white);
		pocitadloKecup.setHorizontalAlignment(JLabel.LEFT);
		pocitadloKecup.setIcon(kecup);
		this.pocitadloKecup = pocitadloKecup;
		okno.odpovediPanel.add(pocitadloKecup);

		JLabel pocitadloStopky = new JLabel();
		pocitadloStopky.setLayout(null);
		pocitadloStopky.setBounds(RelatvniVelikost.obdelnik(85, 0, 13, 60, okno.odpovediPanel.getWidth(), okno.odpovediPanel.getHeight()));
		pocitadloStopky.setFont(new Font("Consolas", Font.PLAIN, 35));
		pocitadloStopky.setForeground(Color.white);
		pocitadloStopky.setHorizontalAlignment(JLabel.RIGHT);
		pocitadloStopky.setHorizontalTextPosition(JLabel.LEFT);
		pocitadloStopky.setVisible(true);
		pocitadloStopky.setIcon(clock);
		this.pocitadloStopky = pocitadloStopky;
		okno.odpovediPanel.add(pocitadloStopky);
		// tlacitka dole
		startTlacitko.setLayout(null);
		startTlacitko.setBounds(RelatvniVelikost.obdelnik(40, 10, 20, 80, okno.odpovediPanel.getWidth(), okno.odpovediPanel.getHeight()));
		startTlacitko.setFont(new Font("Consolas", Font.BOLD, 25));
		startTlacitko.setText("START");
		startTlacitko.setBackground(Color.green);
		startTlacitko.setOpaque(true);
		startTlacitko.setHorizontalAlignment(JLabel.CENTER);
		okno.odpovediPanel.add(startTlacitko);

		// start button
		okno.odpovediPanel.add(startTlacitko);
		startTlacitko.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				JButton b = (JButton) e.getSource();
				ButtonModel m = b.getModel();
				boolean isPressed = m.isPressed();
				if (isPressed) {
					startTlacitko.setVisible(false);
					level2.setCas(System.currentTimeMillis());
					startPanel();
					startTlacitko.removeChangeListener(this);
				}
			}
		});

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.gray);
		g2d.fillRect(0, 0, this.getWidth(), getHeight());

		vozikimg.paintIcon(this, g, vozik.misto, vozik.souradniceY);

		for (int i = 0; i < pult.size(); i++) {
			if (pult.get(i).jmeno.equals("drozdi")) {
				drozdi.paintIcon(this, g, pult.get(i).x, pult.get(i).y);
			}
			if (pult.get(i).jmeno.equals("kecup")) {
				kecup.paintIcon(this, g, pult.get(i).x, pult.get(i).y);
			}
		}
		if (stopky >= 0) {
			String sekundyText = Long.toString((stopky % 3600) / 60);
			if ((stopky % 3600) / 60 < 10) {
				sekundyText = "0" + sekundyText;
			}
			String milisekundyText = Long.toString(stopky % 60);
			if (stopky % 60 < 10) {
				milisekundyText = "0" + milisekundyText;
			}
			this.pocitadloStopky.setText(stopky / 3600 + ":" + sekundyText + ":" + milisekundyText);
		} else {
			if (konecDrozdi % 20 == 0) {
				radaDrozdi();
			}
			konecDrozdi++;
		}
		this.pocitadloDrozdi.setText(pocetDrozdi + "/" + drozdiMax);
		this.pocitadloKecup.setText("" + pocetKecup);
		if (pocetDrozdi >= drozdiMax) {
			hraThread = null;
		}
		g2d.dispose();

	}

	int FPS = 120;

	@Override
	public void run() {
		double intervalCas = 1000000000 / FPS; // prevod jednotek
		double delta = 0;
		long minulyCas = System.nanoTime();
		long cas;

		while (hraThread != null) {
			cas = System.nanoTime();
			delta += (cas - minulyCas) / intervalCas;
			minulyCas = cas;

			if (delta > 1) {
				update();
				repaint();
				delta--;
				stopky--;
			}
		}
		konecnyScreen();
		System.out.println("KONEC Panelu  " + Thread.currentThread());
	}

	public void konecnyScreen() {
		mistniOkno.otazkyLabel.setText("Jsi toho nakoupil nějak moc, to nebude jen na vaření...");

		startTlacitko.setText("JÍT Z OBCHODU");
		startTlacitko.setBackground(Color.cyan);
		startTlacitko.setVisible(true);
		startTlacitko.addChangeListener(new ChangeListener() {

			@Override
			synchronized public void stateChanged(ChangeEvent e) {
				JButton b = (JButton) e.getSource();
				ButtonModel m = b.getModel();
				boolean isPressed = m.isPressed();
				if (isPressed) {
					startTlacitko.setVisible(false);
					startTlacitko.removeChangeListener(this);
					System.out.println("KONEC tlacitko  " + Thread.currentThread());
					level2.ulozeniCasu();
					konec();
				}
			}

		});
	}
	public void konec() {
		synchronized (Level2.t) {
			try {
				mistniOkno.smazat();
				Level2.t.notify();
			} catch (Exception e2) {
				System.out.println("CHYBA  -- KONEC tlacitko" + Thread.currentThread());
			}
		}
	}
	private void update() {
		// vozik pohyb
		if (keyK.vozikJede == true) {
			if (vozik.otocenDoPrava == true) {
				vozikimg = vozik.imgR;
				vozik.misto += vozik.rychlost;

				if (vozik.misto + vozik.sirka > this.getWidth()) {
					vozik.misto = this.getWidth() - vozik.sirka;
				}
			}
			if (vozik.otocenDoPrava == false) {
				vozik.misto -= vozik.rychlost;
				vozikimg = vozik.imgL;
				if (vozik.misto < 0) {
					vozik.misto = 0;
				}
			}
		}
		// generace jidla
		if (random.nextInt(20) == 19) {
			switch (random.nextInt(2)) {
			case 0: {
				pult.add(new Jidlo("drozdi", random.nextInt(40) * Jidlo.velikost, -Jidlo.velikost));
			}
			case 1: {
				pult.add(new Jidlo("kecup", random.nextInt(40) * Jidlo.velikost, -Jidlo.velikost));
			}
			}
		}
		// jidlo pada
		for (int i = 0; i < pult.size(); i++) {
			pult.get(i).y += Jidlo.rychlost;
			if (pult.get(i).y > this.getHeight()) {
				// System.out.println("zniceno");
				pult.remove(i);
			} else if (pult.get(i).kolize(vozik)) {
				switch (pult.get(i).jmeno) {
				case "drozdi": {
					pocetDrozdi++;
				}
				case "kecup": {
					pocetKecup++;
				}
				}
				pult.remove(i);
			}
		}
	}

	public void startPanel() {
		if (hraThread == null) {
			hraThread = new Thread(this);
			hraThread.start();
			System.out.println("Thread PANEL začal " + Thread.currentThread());
		}
	}

	public void radaDrozdi() {
		for (int i = 0; i < 40; i++) {
			pult.add(new Jidlo("drozdi", i * Jidlo.velikost, -Jidlo.velikost));
		}
	}
}

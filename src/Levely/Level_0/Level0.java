package Levely.Level_0;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;

import main.NesnupejteDrozdi;
import main.Okno;
import main.RelatvniVelikost;

public class Level0 {
	Okno okno;
	Thread t;
	FileManager fileManager;

	public Level0(Okno okno) {
		this.okno = okno;
		fileManager = new FileManager();
		t = Thread.currentThread();
		zaklad(okno);
		// okno.setVisible(true);
		okno.repaint();
		// cekani dokud nezmackne konecna tlacitko
		synchronized (this) {
			try {
				this.wait();
			} catch (InterruptedException ex) {
				System.out.println("CHYBA -- Level0");
			}

		}
		okno.smazat();
		System.out.println("KONEC Level0");
	}

	void zaklad(Okno okno) {
		okno.smazat();
		okno.defOkno();

		okno.hlPanel.setBounds(RelatvniVelikost.obdelnik(0, 0, 100, 100));
		okno.hlPanel.setLayout(null);
		okno.hlPanel.setOpaque(false);
		okno.add(okno.hlPanel);
		//
		okno.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
            	/** ulozeni casu do txt dokumentu **/
            	ulozitCasy();
                System.exit(0);
            }
        });
		
		// pozadi
		JLabel pozadi = new JLabel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.drawImage(FileManager.nesnupejteDrozdi, 0, 0, this.getSize().width, this.getSize().height, null);
			}
		};
		pozadi.setBounds(RelatvniVelikost.obdelnik(0, 0, 100, 100));
		pozadi.setOpaque(false);
		okno.hlPanel.add(pozadi);

		// levely tlacitka
		pozadi.add(tlacitkoLevel(pozadi, RelatvniVelikost.obdelnik(10, 35, 20, 10), 1));
		pozadi.add(tlacitkoLevel(pozadi, RelatvniVelikost.obdelnik(10, 50, 20, 10), 2));
		pozadi.add(tlacitkoLevel(pozadi, RelatvniVelikost.obdelnik(10, 65, 20, 10), 3));
		// level3 mapy
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(15, 80, 7, 10), 0));
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(25, 80, 7, 10), 1));
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(35, 80, 7, 10), 2));
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(45, 80, 7, 10), 3));
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(55, 80, 7, 10), 4));
		pozadi.add(tlacitkoLevel3Mapa(pozadi, RelatvniVelikost.obdelnik(65, 80, 7, 10), 5));
		// cas a statictiky
		JLabel statistika = new JLabel() {
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setColor(java.awt.Color.blue);
				g2d.fillRect(0, 0, this.getSize().width, this.getSize().height);
				// g2d.drawImage(FileManager.nesnupejteDrozdi, 0, 0, this.getSize().width,
				// this.getSize().height, null);
				g2d.setColor(java.awt.Color.yellow);
				g.setFont(g.getFont().deriveFont(20f));
				g.drawString("Účet: " + NesnupejteDrozdi.ucet, 20, 30);
				g.setFont(g.getFont().deriveFont(12f));
				g.drawString("Časy:", 20, 100);

				g2d.drawString("Level1:           " + prevodCasu(NesnupejteDrozdi.casLevel1), 20, 120);
				g2d.drawString("Level2:           " + prevodCasu(NesnupejteDrozdi.casLevel2), 20, 140);
				for (int i = 0; i < NesnupejteDrozdi.mapaCasy.length; i++) {
					g2d.drawString("Level3/mapa" + i + ":       " + prevodCasu(NesnupejteDrozdi.mapaCasy[i]), 20, 160 + 20 * i);

				}
			}
		};
		statistika.setBounds(RelatvniVelikost.obdelnik(35, 35, 20, 40));

		// zmena uctu
		JTextField novyUcet = new JTextField();
		novyUcet.setBounds(RelatvniVelikost.procentoX(10, statistika.getWidth()), 50,
				RelatvniVelikost.procentoX(60, statistika.getWidth()), 20);
		statistika.add(novyUcet);
		novyUcet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ulozitCasy();
				NesnupejteDrozdi.ucet = novyUcet.getText();
				NesnupejteDrozdi.ucetCesta = "src/ucty/" + NesnupejteDrozdi.ucet + ".json";
				File fileCheck = new File(Okno.ziskatCestu(NesnupejteDrozdi.ucetCesta));
				if (fileCheck.exists() && !fileCheck.isDirectory()) {
					nacistCasy(statistika);
				}else {
					if (jeplatne(novyUcet.getText()) && !novyUcet.getText().isEmpty()) {
						zalozitUcet(NesnupejteDrozdi.ucet);
						nacistCasy(statistika);
					}else {
						NesnupejteDrozdi.ucet = "NeplatnyZnaky";
						NesnupejteDrozdi.ucetCesta = "src/ucty/" + NesnupejteDrozdi.ucet + ".json";
						nacistCasy(statistika);
					}
				}
				NesnupejteDrozdi.ucetCesta = "src/ucty/" + NesnupejteDrozdi.ucet + ".json";
				novyUcet.setText("");
				statistika.repaint();
				nacistCasy(statistika);

			}
		});
		pozadi.add(statistika);
		// discord talcitko
		JButton discordTlacitko = new JButton() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				BufferedImage img = null;
				try {
					img = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level0/discord.png")));
				} catch (Exception e) {
					System.out.println("CHYBA -- Level0 -- Nacteni obrazku dircord" + e);
				}
				g2d.drawImage(img, 0, 0, this.getSize().width, this.getSize().height, null);
			}

		};
		discordTlacitko.addActionListener(e -> {
			openWebpage("https://discord.gg/AzBmaPWQGR");
		});
		discordTlacitko.setOpaque(false);
		discordTlacitko.setBounds(RelatvniVelikost.obdelnik(90, 80, 7, 10));
		discordTlacitko.setVisible(true);
		pozadi.add(discordTlacitko);
		/**
		 * //ulozit tlacitko JButton ulozitTlacitko = new JButton() {
		 * 
		 * @Override public void paintComponent(Graphics g) { Graphics2D g2d =
		 * (Graphics2D) g; BufferedImage img = null; try { img = ImageIO.read(new
		 * File(Okno.ziskatCestu("rsc/Level0/save.png"))); } catch (Exception e) {
		 * System.out.println("CHYBA -- Level0 -- Nacteni obrazku save" + e); }
		 * g2d.drawImage(img, 0, 0, this.getSize().width, this.getSize().height, null);
		 * }
		 * 
		 * }; ulozitTlacitko.addActionListener(e -> { ulozitCasy(); });
		 * ulozitTlacitko.setOpaque(false);
		 * ulozitTlacitko.setBounds(RelatvniVelikost.obdelnik(90, 65, 7, 10));
		 * ulozitTlacitko.setVisible(true);
		 * 
		 * pozadi.add(ulozitTlacitko);
		 **/
	}

	protected void zalozitUcet(String ucet) { //nastaveni noveho uctu
		JsonObject jo1 = new JsonObject();
		jo1.put("Level1", 2147483647);
		jo1.put("Level2", 2147483647);
		for (int i = 0; i < NesnupejteDrozdi.mapaCasy.length; i++) {
			jo1.put("Level3__" + i, 2147483647);
		}
		String jsonString = Jsoner.serialize(jo1);
		try {
			Files.write(Paths.get(Okno.ziskatCestu(NesnupejteDrozdi.ucetCesta)), jsonString.getBytes(), StandardOpenOption.CREATE);
			System.out.println("ucet {"+NesnupejteDrozdi.ucet +"} nove zalozen");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void ulozitCasy() {
		JsonObject jo = new JsonObject();
		jo.put("Level1", NesnupejteDrozdi.casLevel1);
		jo.put("Level2", NesnupejteDrozdi.casLevel2);
		for (int i = 0; i < NesnupejteDrozdi.mapaCasy.length; i++) {
			jo.put("Level3__" + i, NesnupejteDrozdi.mapaCasy[i]);
		}
		String jsonString = Jsoner.serialize(jo);
		try {
			Files.delete(Paths.get(Okno.ziskatCestu(NesnupejteDrozdi.ucetCesta)));
			Files.write(Paths.get(Okno.ziskatCestu(NesnupejteDrozdi.ucetCesta)), jsonString.getBytes(), StandardOpenOption.CREATE);
			System.out.println("ulozen ucet {" + NesnupejteDrozdi.ucet+"}");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void nacistCasy(JLabel statistika) {
		JsonObject jo = null;
		String jsonString = null;
		File fileCheck = new File(Okno.ziskatCestu(NesnupejteDrozdi.ucetCesta));
			try {
				jsonString = new String(Files.readAllBytes(Paths.get(Okno.ziskatCestu(NesnupejteDrozdi.ucetCesta))));
			} catch (IOException e) {
				// e.printStackTrace();
		}
		try {
			jo = (JsonObject) Jsoner.deserialize(jsonString);
			NesnupejteDrozdi.casLevel1 = ((BigDecimal) jo.get("Level1")).longValue();
			NesnupejteDrozdi.casLevel2 = ((BigDecimal) jo.get("Level2")).longValue();
			for (int i = 0; i < NesnupejteDrozdi.mapaCasy.length; i++) {
				NesnupejteDrozdi.mapaCasy[i] = ((BigDecimal) jo.get("Level3__" + i)).longValue();
			}
			statistika.repaint();
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("nacitani uctu {" + Main.ucet +"} dokonceno");
		}
	}

	// zkopcena metoda jestli je string validni
	public static boolean jeplatne(String filename) {
		try {
			Paths.get(filename);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private JButton tlacitkoLevel3Mapa(JLabel pozadi, Rectangle rect, int mapa) {
		// tlacitko Levely
		JButton tlacitko = new JButton() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				BufferedImage img = null;
				try {
					img = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level0/mapa" + mapa + ".png")));
				} catch (Exception e) {
					System.out.println("CHYBA -- Level0 -- Nacteni mapy " + mapa + " " + e);
				}
				g2d.drawImage(img, 0, 0, this.getSize().width, this.getSize().height, null);
			}

		};
		tlacitko.addActionListener(e -> {
			NesnupejteDrozdi.postup = 3;
			NesnupejteDrozdi.level3Level = mapa;
			konec();
		});
		okno.hlPanel.setOpaque(false);
		tlacitko.setBounds(rect);
		tlacitko.setVisible(true);
		// tlacitko.repaint();
		return tlacitko;
	}

	public void konec() {
		synchronized (this) {
			notify();
		}

	}

	private JButton tlacitkoLevel(JLabel label, Rectangle rect, int cisloLevelu) {
		// tlacitko Levely
		JButton tlacitko = new JButton() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				BufferedImage img = null;
				try {
					img = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level0/level" + cisloLevelu + ".png")));
				} catch (Exception e) {
					System.out.println("CHYBA -- Level0 -- Nacteni levelu " + cisloLevelu + " " + e);
				}
				g2d.drawImage(img, 0, 0, this.getSize().width, this.getSize().height, null);
			}

		};
		tlacitko.addActionListener(e -> {
			NesnupejteDrozdi.postup = cisloLevelu;
			konec();
		});
		okno.hlPanel.setOpaque(false);
		tlacitko.setBounds(rect);
		tlacitko.setVisible(true);
		// tlacitko.repaint();
		return tlacitko;
	}

	private String prevodCasu(long cas) {
		if (cas == 2147483647) {
			return "??????";
		}
		// String vysledek = null;
		String sekundyText = Long.toString((cas % 60000) / 1000);
		if ((cas % 60000) / 1000 * 60 < 10) {
			sekundyText = "0" + sekundyText;
		}
		String milisekundyText = Long.toString((cas % 60000) % 1000);
		if ((cas % 60000) % 1000 < 10) {
			milisekundyText = "0" + milisekundyText;
		}
		return (cas / 60000 + ":" + sekundyText + ":" + milisekundyText);
	}

	// zkopcena metoda na otvirani
	public static void openWebpage(String urlString) {
		try {
			java.awt.Desktop.getDesktop().browse(new URL(urlString).toURI());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

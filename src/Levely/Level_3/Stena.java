package Levely.Level_3;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

public class Stena {
		//Image img = new File("somefile.png");
		protected static Panel_level3 panel;
		protected Point pozice;
		protected Point velikost;

		Point2D.Double rychlost;
		final Point2D.Double rychlostMax = new Point2D.Double(10, 10);
		public Rectangle hitBox;

		boolean left, right, up, down;
		
		static BufferedImage stena;
		@SuppressWarnings("static-access")
		public Stena(int x,int y,int velikostx,int velikosty,Panel_level3 panel) {
			this.panel = panel;
			pozice = new Point(x,y);
			velikost = new Point(velikostx, velikosty);
			hitBox = new Rectangle(this.pozice.x- panel.posun.x, this.pozice.y, velikost.x, velikost.y);
			
		}

		public void nastav() {
			hitBox = new Rectangle(pozice.x - panel.posun.x, pozice.y, velikost.x, velikost.y);
		}

		public void draw(Graphics2D g2d,Rectangle r) {
			if (this.hitBox.intersects(r)) { //aby se rendrovali jen veci na obrazovce
				g2d.setColor(Color.gray);
				g2d.drawImage(FileManager.stena, hitBox.x, hitBox.y, velikost.x, velikost.y, null);
			}
		}


	}


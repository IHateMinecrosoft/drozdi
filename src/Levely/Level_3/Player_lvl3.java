package Levely.Level_3;

import java.awt.Point;
import java.awt.Color;
import java.awt.geom.Point2D;
import Levely.Level_3.steny.Bodak;
import Levely.Level_3.steny.Checkpoint;
import Levely.Level_3.steny.Dvere;
import Levely.Level_3.steny.Klic;
import Levely.Level_3.steny.Zebrik;
import main.NesnupejteDrozdi;
import main.Test;

import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.FontMetrics;

public class Player_lvl3 {
	Panel_level3 panel;
	public Point pozice;
	Point velikost;

	Point2D.Double rychlost;
	Point2D.Double rychlostMax = new Point2D.Double(4, 6);
	public Rectangle hitBox;

	boolean left, right, up, down;
	boolean NaZemi = false;
	int otoceni = 1; // 0- nahoru, 1- pravo, 2 - dolu , 3 - levo
	int vystrel = 0;
	public boolean strilet = false;

	public Player_lvl3(Point poziceProtiOknu, Panel_level3 panel) {
		this.panel = panel;
		velikost = new Point((int) (0.9 * panel.velikostBunky), (int) (0.9 * panel.velikostBunky));
		rychlost = new Point2D.Double(0, 0);
		if (poziceProtiOknu == null) {
			this.pozice = new Point(panel.getX() + panel.getWidth() / 2 - velikost.x / 2,
					panel.getY() + panel.getHeight() / 2 - velikost.y / 2);
		} else {
			this.pozice = poziceProtiOknu;
		}
		hitBox = new Rectangle(this.pozice.x, this.pozice.y, velikost.x, velikost.y);
	}

	public void nastav() {
		double srovanani = panel.velikostBunky/30;
		if (NaZemi) {
			rychlostMax = new Point2D.Double(6*srovanani, 6*srovanani);
		} else {
			rychlostMax = new Point2D.Double(4*srovanani, 6*srovanani);
		}
		// nastaveni pohybu X
		if (left && right || !left && !right)
			rychlost.x *= 0.8;
		else if (left && !right)
			rychlost.x = rychlost.x -1*srovanani;
		else if (!left && right)
			rychlost.x = rychlost.x + 1*srovanani;;
		///rychlost.x = rychlost.x*srovanani;
		//limitovani ryvhlosti X
		if (rychlost.x < 0.1*srovanani && rychlost.x > -0.1*srovanani) {
			rychlost.x = 0;
		}
		if (rychlost.x > rychlostMax.x) {
			rychlost.x = rychlostMax.x;
		} else if (rychlost.x < -rychlostMax.x) {
			rychlost.x = -rychlostMax.x;
		}

		if (up && NaZemi == true) {
			rychlost.y = -1.5*rychlostMax.y;
			NaZemi = false;
		}
		rychlost.y += 0.3*srovanani;
		// x kolize
		//rychlost.x = rychlost.x*srovanani; 
		hitBox.x += rychlost.x;
		for (Stena stena : panel.stenyNaObrazovce) {
			if (this.hitBox.intersects(stena.hitBox)) {
				hitBox.x -= rychlost.x;
				while (!stena.hitBox.intersects(hitBox))
					hitBox.x += Math.signum(rychlost.x);
				hitBox.x -= Math.signum(rychlost.x);
				rychlost.x = 0;
				pozice.x = hitBox.x;
			}
		}
		// zebriky
		NaZemi = false;
		for (Zebrik zebrik : panel.zebriky) {
			if (this.hitBox.intersects(zebrik.hitBox)) {
				NaZemi = true;
				otoceni = 0;
				if (up) {
					rychlost.y = -rychlostMax.y;
				} else if (down) {
					rychlost.y = rychlostMax.y / 2;
				} else {
					rychlost.y = 0;
				}
			}
		}
		// y kolize
		//rychlost.y = rychlost.y*srovanani; 
		hitBox.y += rychlost.y;
		for (Stena stena : panel.stenyNaObrazovce) {
			if (this.hitBox.intersects(stena.hitBox)) {
				hitBox.y -= rychlost.y;
				while (!stena.hitBox.intersects(hitBox))
					hitBox.y += Math.signum(rychlost.y);
				hitBox.y -= Math.signum(rychlost.y);
				if (rychlost.y > 0)
					NaZemi = true;
				rychlost.y = 0;
				pozice.y = hitBox.y;
			}
		}
		// nasteveni otoceni
		if (rychlost.x > 0) {
			otoceni = 1;
		}
		if (rychlost.x < 0) {
			otoceni = 3;
		}
		
		
		pozice.y += rychlost.y;

		hitBox.x = pozice.x;
		hitBox.y = pozice.y;

		panel.posun.x += rychlost.x;
		panel.posun.y += rychlost.y;
		if (down) {
			otoceni = 2;
		}
		vystrel();
		// zjistuje zda byl zasazena
		for (Strela strela : panel.strelyEntit) {
			if (hitBox.intersects(strela.hitBox)) {
				panel.restart();
			}
		}
		// zjistuje zda dopad na bodak
		for (Bodak bodak : panel.bodaky) {
			bodak.kontrolaKolize(this);
		}
		// zjistuje zda je na checkpointu
		for (Checkpoint checkpoint : panel.checkpointy) {
			checkpoint.kontrolaKolize(this);
		}
		//zjistuje zda sebral klic
		for (Klic klic : panel.klice) {
			klic.kontrolaKolize(this);
		}
		//zda šel na dvere
		for (Dvere dvere : panel.dvereList) {
			dvere.kontrolaKolize(this);
		}
	}

	@SuppressWarnings("unused")
	public void draw(Graphics2D g2d) {
		g2d.setColor(Color.cyan);
		FontMetrics fm = g2d.getFontMetrics();
	    int x = pozice.x + this.velikost.x /2 -(fm.stringWidth(NesnupejteDrozdi.ucet)) / 2;
		g2d.drawString(NesnupejteDrozdi.ucet, x, pozice.y -10);
		//g2d.drawString((pozice.x + panel.posun.x) + ";" + (pozice.y + panel.posun.y) + "(" + panel.strelyHrace.size()
		//		+ ")" + "(" + panel.strelyEntit.size() + ")\n+ " + "(" + NaZemi + ")", pozice.x, pozice.y);
		if (otoceni == 1) {
			g2d.drawImage(FileManager.hracR, pozice.x, pozice.y, velikost.x, velikost.y, null);
		} else if (otoceni == 3) {
			g2d.drawImage(FileManager.hracL, pozice.x, pozice.y, velikost.x, velikost.y, null);
		} else if (otoceni == 0) {
			g2d.drawImage(FileManager.hracU, pozice.x, pozice.y, velikost.x, velikost.y, null);
		} else if (otoceni == 2) {
			g2d.drawImage(FileManager.hracD, pozice.x, pozice.y, velikost.x, velikost.y, null);
		}
		// hitbox
		if (Test.hitBoxHrac == true) {
			g2d.setColor(Color.green);
			g2d.draw(hitBox);
		}

	}

	public synchronized void vystrel() {
		vystrel++;
		if (vystrel > 20 && (strilet == true || otoceni == 2)) {
			vystrel = 0;
			panel.strelyHrace.add(new Strela(panel, this));
		}
	}

}

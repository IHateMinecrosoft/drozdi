package Levely.Level_3;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

import Levely.Level_3.steny.Slimak;
import Levely.Level_3.steny.Vez;
import main.Test;

public class Strela {
	public static Point velikost = new Point(20, 20);;
	public Point2D.Double pozice;
	private Panel_level3 panel;
	Point2D.Double rychlost;
	static int rychlostStrely = 10;
	int typStrely; // 1 je od hrace, 2 je od veze
	public Rectangle hitBox;

	public Strela(Panel_level3 panel, Player_lvl3 player) {
		typStrely = 1;
		this.panel = panel;
		pozice = new Point2D.Double(player.pozice.x + panel.posun.x + player.velikost.x / 2 - velikost.x / 2,
				player.pozice.y + player.velikost.y / 2 - velikost.y / 2);
		if (player.otoceni == 2) {
			rychlost = new Point2D.Double(0, rychlostStrely);
		} else if (player.otoceni == 1) {
			rychlost = new Point2D.Double(rychlostStrely, 0);
		} else if (player.otoceni == 0) {
			rychlost = new Point2D.Double(0, -rychlostStrely);
		} else if (player.otoceni == 3) {
			rychlost = new Point2D.Double(-rychlostStrely, 0);
		}
		hitBox = new Rectangle((int) (pozice.x - panel.posun.x), (int) (pozice.y), velikost.x, velikost.y);
	}

	public Strela(Panel_level3 panel, Vez vez, Player_lvl3 player) {
		typStrely = 2;
		this.panel = panel;
		pozice = new Point2D.Double(vez.pozice.x + panel.velikostBunky - velikost.x / 2,
				vez.pozice.y + panel.velikostBunky - velikost.y / 2);
		double x = player.hitBox.x + player.velikost.x / 2 - (vez.hitBox.x + vez.hitBox.width / 2);
		double y = player.hitBox.y + player.velikost.y / 2 - (vez.hitBox.y + vez.hitBox.height / 2);
		double pomer = (rychlostStrely / Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
		rychlost = new Point2D.Double.Double(pomer * x, pomer * y);
		hitBox = new Rectangle((int) (pozice.x - panel.posun.x), (int) (pozice.y), velikost.x, velikost.y);
	}

	public Strela(Panel_level3 panel, Slimak slimak, Player_lvl3 player) {
		typStrely = 3;
		this.panel = panel;
		pozice = new Point2D.Double(slimak.pozice.x + panel.velikostBunky - velikost.x / 2,
				slimak.pozice.y + panel.velikostBunky - velikost.y );
		double x = player.hitBox.x + player.velikost.x / 2 - (slimak.hitBox.x + slimak.hitBox.width / 2);
		double y = player.hitBox.y - (slimak.hitBox.y + slimak.hitBox.height / 2);
		double pomer = (rychlostStrely / Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
		rychlost = new Point2D.Double.Double(pomer * x*1.25, pomer * y*1.25);
		hitBox = new Rectangle((int) (pozice.x - panel.posun.x), (int) (pozice.y), velikost.x, velikost.y);
	}

	void nastav() {

		pozice.x = pozice.x + rychlost.x;
		pozice.y = pozice.y + rychlost.y;

		for (Stena stena : panel.stenyNaObrazovce) {
			if (stena.hitBox.intersects(hitBox)) {
				switch (typStrely) {
				case 1: {
					panel.strelyHrace.remove(this);
					break;
				}
				case 3: {
					panel.strelyEntit.remove(this);
					break;
				}
				}
			}
		}
		hitBox = new Rectangle((int) (pozice.x - panel.posun.x), (int) (pozice.y), velikost.x, velikost.y);
	}

	@SuppressWarnings("unused")
	void draw(Graphics2D g2d, Rectangle r) {
		g2d.setColor(Color.cyan);
		if (typStrely == 1) {
			g2d.drawImage(FileManager.strela, hitBox.x, hitBox.y, velikost.x, velikost.y, null);
		} else if (typStrely == 2) {
			g2d.setColor(Color.red);
			g2d.fillRect(hitBox.x, hitBox.y, velikost.x, velikost.y);
		}
		else if (typStrely == 3) {
			g2d.setColor(Color.cyan);
			g2d.fillRect(hitBox.x, hitBox.y, velikost.x, velikost.y);
		}
		if (Test.hitBoxStrely == true) {
			g2d.setColor(Color.green);
			g2d.draw(hitBox);
		}
	}
}

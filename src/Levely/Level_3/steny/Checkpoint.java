package Levely.Level_3.steny;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

import Levely.Level_3.FileManager;
import Levely.Level_3.Panel_level3;
import Levely.Level_3.Player_lvl3;
import Levely.Level_3.Stena;

public class Checkpoint extends Stena {
	public Checkpoint(int x, int y, int velikostx, int velikosty, Panel_level3 panel) {
		super(x, y, velikostx, velikosty, panel);
	}

	public void nastav() {
		hitBox = new Rectangle(pozice.x - panel.posun.x, pozice.y - 1, velikost.x, velikost.y + 1);
	}

	public void kontrolaKolize(Player_lvl3 player) {
		if (player.hitBox.intersects(hitBox)) {
			//panel.level3.posun.x =   panel.level3.posun.x + (this.hitBox.x - player.hitBox.x)
			panel.level3.posun.x = panel.posun.x+ (this.hitBox.x - player.hitBox.x);
			panel.level3.PocatecniPozice.x = super.pozice.x / panel.velikostBunky ;
			panel.level3.PoziceOprotiObrazovce.y = super.pozice.y / panel.velikostBunky;
		}
	}

	public void draw(Graphics2D g2d, Rectangle r) {
		if (this.hitBox.intersects(r)) {
			g2d.drawImage(FileManager.checkpoint, hitBox.x, hitBox.y, velikost.x, velikost.y, null);
		}
	}

}

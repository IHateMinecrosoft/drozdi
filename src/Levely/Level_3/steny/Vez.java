package Levely.Level_3.steny;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import Levely.Level_3.FileManager;
import Levely.Level_3.Panel_level3;
import Levely.Level_3.Player_lvl3;
import Levely.Level_3.Stena;
import Levely.Level_3.Strela;
import main.Test;
import Levely.Level_3.Panel_level3;

public class Vez extends Stena {
	Panel_level3 panel;
	int vystrel = 0;

	public Vez(int x, int y, int velikostx, int velikosty, Panel_level3 panel) {
		super(x, y, velikostx * 2, velikosty * 2, panel);
		hitBox = new Rectangle(this.pozice.x - panel.posun.x+panel.velikostBunky/2, this.pozice.y, velikost.x-panel.velikostBunky, velikost.y);
		this.panel = panel;
	}

	public void nastav(Player_lvl3 player) {
		hitBox = new Rectangle(this.pozice.x - panel.posun.x+panel.velikostBunky/2, this.pozice.y, velikost.x-panel.velikostBunky, velikost.y);
		if (this.hitBox.intersects(panel.obrazovka)) {
			vystrel++;
			if (vystrel > 80) {
				//System.out.println("adwawd");
				vystrel = 0;
				panel.strelyEntit.add(new Strela(panel, this, player));
			}
			//zjistuje zda byla zasazena
			for (Strela strela : panel.strelyHrace) {
				if (hitBox.intersects(strela.hitBox)) {
					panel.strelyHrace.remove(strela);
					panel.veze.remove(this);
				}
			}
		}
		}

	public void draw(Graphics2D g2d, Rectangle r) {
		if (this.hitBox.intersects(r)) {
			//g2d.setColor(Color.cyan);
			//g2d.fillRect(hitBox.x, hitBox.y, velikost.x, velikost.y);
			g2d.drawImage(FileManager.vez,	hitBox.x - panel.velikostBunky/2, hitBox.y, velikost.x, velikost.y, null);
			//g2d.setColor(Color.red);
			//g2d.fillRect(pozice.x - panel.posun.x, pozice.y, velikost.x, velikost.y);
		}
		if (Test.hitBoxVeze == true) {
			g2d.setColor(Color.green);
			g2d.draw(hitBox);
		}
	}
}

package Levely.Level_3.steny;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import Levely.Level_3.FileManager;
import Levely.Level_3.Panel_level3;
import Levely.Level_3.Stena;

public class Zebrik extends Stena {

	public Zebrik(int x, int y, int velikostx, int velikosty, Panel_level3 panel) {
		super(x, y, velikostx, velikosty, panel);
	}

	public void draw(Graphics2D g2d, Rectangle r) {
		if (this.hitBox.intersects(r)) {
			g2d.drawImage(FileManager.zebrik, hitBox.x, hitBox.y, velikost.x, velikost.y, null);
		}
	}
}
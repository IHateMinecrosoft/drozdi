package Levely.Level_3.steny;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

import Levely.Level_3.FileManager;
import Levely.Level_3.Panel_level3;
import Levely.Level_3.Player_lvl3;
import Levely.Level_3.Stena;

public class Klic extends Stena {
	public Klic(int x, int y, int velikostx, int velikosty, Panel_level3 panel) {
		super(x, y, velikostx, velikosty, panel);
	}
	public void kontrolaKolize(Player_lvl3 player) {
		if (player.hitBox.intersects(hitBox)) {
			panel.klice.remove(this);
			panel.level3.pocetKlicu++;
		}
	}

	public void draw(Graphics2D g2d, Rectangle r) {
		if (this.hitBox.intersects(r)) {
			g2d.drawImage(FileManager.klic, hitBox.x, hitBox.y, velikost.x, velikost.y, null);
		}
	}

}

package Levely.Level_3.steny;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import Levely.Level_3.FileManager;
import Levely.Level_3.Panel_level3;
import Levely.Level_3.Player_lvl3;
import Levely.Level_3.Stena;

public class Bodak extends Stena {
	public Bodak(int x, int y, int velikostx, int velikosty, Panel_level3 panel) {
		super(x, y, velikostx, velikosty, panel);

	}

	public void draw(Graphics2D g2d, Rectangle r) {
		if (this.hitBox.intersects(r)) {
			g2d.drawImage(FileManager.bodak, hitBox.x, hitBox.y, velikost.x, velikost.y, null);
		}

	}

	public void kontrolaKolize(Player_lvl3 player) {
		if (player.hitBox.intersects(hitBox)) {
			player.hitBox = new Rectangle(0, 0);
			panel.restart();
		}
	}

}

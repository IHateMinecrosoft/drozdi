package Levely.Level_3.steny;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

import Levely.Level_3.FileManager;
import Levely.Level_3.Panel_level3;
import Levely.Level_3.Player_lvl3;
import Levely.Level_3.Stena;
import main.NesnupejteDrozdi;

public class Dvere extends Stena {
	public int maxKlicu;
	public int open;

	public Dvere(int x, int y, int velikostx, int velikosty, Panel_level3 panel, int max) {
		super(x, y, velikostx, velikosty, panel);
		this.maxKlicu = max;
		open = this.maxKlicu;
	}

	public void nastav(Panel_level3 panel) {
		nastav();
		open = maxKlicu - panel.level3.pocetKlicu;
	}

	public void kontrolaKolize(Player_lvl3 player) {
		if (open <= 0) {
			if (player.hitBox.intersects(hitBox)) {
				panel.level3.ulozeniCasu();
				panel.level3.pocetZdechnuti = -1;
				panel.level3.ulozeneKlice = null;
				panel.level3.pocetZdechnuti = -1;
				panel.level3.pocetKlicu = 0;
				panel.level3.konec = false;
				panel.level3.posun.x = 0;
				panel.level3.PoziceOprotiObrazovce = (Point) panel.level3.zakladniPoziceOprotiObrazovce.clone();
				panel.konec();
			}
		}

	}

	public void draw(Graphics2D g2d, Rectangle r) {
		if (this.hitBox.intersects(r)) {
			if (open > 0) {
				g2d.drawImage(FileManager.dvere, hitBox.x, hitBox.y, velikost.x, velikost.y, null);
			}else {
				g2d.drawImage(FileManager.dvereOpen, hitBox.x, hitBox.y, velikost.x, velikost.y, null);
				}
		}
	}
}

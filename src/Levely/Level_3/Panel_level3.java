package Levely.Level_3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import java.awt.Rectangle;

import Levely.Level_3.steny.Bodak;
import Levely.Level_3.steny.Checkpoint;
import Levely.Level_3.steny.Dvere;
import Levely.Level_3.steny.Klic;
import Levely.Level_3.steny.Slimak;
import Levely.Level_3.steny.Vez;
import Levely.Level_3.steny.Zebrik;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.Point;
import java.awt.Color;

import main.Okno;
import main.RelatvniVelikost;
import main.Test;

// https://youtu.be/Icd2gAHDSfY
@SuppressWarnings("serial")
public class Panel_level3 extends JPanel implements ActionListener {

	Player_lvl3 player;
	Timer Casovac;
	public Point posun;
	public long cas;
	boolean skoncit;

	public Set<Stena> steny = new HashSet<>();
	public Set<Bodak> bodaky = new HashSet<>();
	public Set<Zebrik> zebriky = new HashSet<>();
	public Set<Checkpoint> checkpointy = new HashSet<>();
	public Set<Strela> strelyHrace = new HashSet<>();
	public Set<Strela> strelyEntit = new HashSet<>();
	public Set<Vez> veze = new HashSet<>();
	public Set<Slimak> slimaci = new HashSet<>();
	public Set<Dvere> dvereList = new HashSet<>();
	public ArrayList<Klic> klice = new ArrayList<>();
	public Set<Stena> stenyNaObrazovce;

	public int velikostBunky;
	public Rectangle obrazovka;
	public Rectangle obrazovkaPosun;
	public Level3 level3;

	public static BufferedImage mapa;

	public Panel_level3(Okno okno, Level3 level3) {
		skoncit = false;
		this.level3 = level3;
		start();
	}

	private void start() {
		this.setBackground(new Color(45, 25, 33));
		this.setBounds(RelatvniVelikost.obdelnik(0, 0, 100, 85));
		this.setDoubleBuffered(true);
		// velikostBunky = this.getWidth() / 40;
		velikostBunky = (int) (this.getHeight() / 18.25);
		Strela.velikost = new Point((int) (velikostBunky / 2.2), (int) (velikostBunky / 2.2));

		player = new Player_lvl3(new Point(level3.PoziceOprotiObrazovce.x * velikostBunky,
				level3.PoziceOprotiObrazovce.y * velikostBunky), this);
		posun = new Point(level3.posun.x, level3.posun.y);

		obrazovka = new Rectangle(0, 0, this.getWidth(), this.getHeight());
		nastaveniZdi();
		level3.konec = true;

		cas = System.nanoTime();
		Casovac = new Timer();
		Casovac.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					update();
					repaint();
				} catch (Exception e) {
					// Casovac.cancel();
					// System.out.println("KONEC" + e);
				}
			}
		}, 50, 15);
	}

	private void update() {
		stenyNaObrazovce = new HashSet<>();
		for (Stena stena : steny) {
			stena.nastav();
			if (obrazovka.intersects(stena.hitBox)) {
				stenyNaObrazovce.add(stena);
			}
		}
		for (Bodak bodak : bodaky) {
			bodak.nastav();
		}
		for (Zebrik zebrik : zebriky) {
			zebrik.nastav();
		}
		for (Klic klic : klice) {
			klic.nastav();
		}
		for (Checkpoint checkpoint : checkpointy) {
			checkpoint.nastav();
		}
		for (Vez vez : veze) {
			vez.nastav(player);
		}
		for (Slimak slimak : slimaci) {
			slimak.nastav(player);
		}
		for (Dvere dvere : dvereList) {
			dvere.nastav(this);
		}
		player.nastav();
		// strely
		for (Strela strela : strelyHrace) {
			strela.nastav();
			if (!obrazovka.intersects(strela.hitBox)) {
				strelyHrace.remove(strela);
			}
		}
		for (Strela strela : strelyEntit) {
			strela.nastav();
			if (!obrazovka.intersects(strela.hitBox)) {
				strelyEntit.remove(strela);
			}
		}

		updateInfo();

	}

	private void updateInfo() {
		level3.odpovediLabel.setText(
				"Úmrtí v důsledku závislosti:" + level3.pocetZdechnuti + "  Klíče: " + level3.pocetKlicu + "/5");

	}

	private void nastaveniZdi() {
		// nastaveni barev z obrazkovepalety
		int stenaBarva = FileManager.paleta.getRGB(0, 0);
		int smrt = FileManager.paleta.getRGB(1, 0);
		int zebrik = FileManager.paleta.getRGB(2, 0);
		int vez = FileManager.paleta.getRGB(3, 0);
		int checkpoint = FileManager.paleta.getRGB(4, 0);
		int klic = FileManager.paleta.getRGB(5, 0);
		int dvere = FileManager.paleta.getRGB(6, 0);
		int slimak = FileManager.paleta.getRGB(7, 0);
		// System.out.println(stenaBarva);
		try {
			mapa = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level3/mapy/mapa" + level3.mapa + ".bmp")));
		} catch (Exception e) {
			System.out.println("CHYBA -- Level3 -- Nacteni obrazku mapy " + e);
		}

		for (int y = 0; y < mapa.getHeight(); y++) {
			for (int x = 0; x < mapa.getWidth(); x++) {
				int color = mapa.getRGB(x, y);
				if (color == stenaBarva) {
					steny.add(new Stena(x * velikostBunky, y * velikostBunky, velikostBunky, velikostBunky, this));
				} else if (color == smrt) {
					bodaky.add(new Bodak(x * velikostBunky, y * velikostBunky, velikostBunky, velikostBunky, this));
				} else if (color == zebrik) {
					zebriky.add(new Zebrik(x * velikostBunky, y * velikostBunky, velikostBunky, velikostBunky, this));
				} else if (color == vez) {
					veze.add(new Vez(x * velikostBunky, y * velikostBunky, velikostBunky, velikostBunky, this));
				} else if (color == checkpoint) {
					checkpointy.add(new Checkpoint(x * velikostBunky, (int) (y * velikostBunky + velikostBunky * 0.95),
							velikostBunky, (int) (velikostBunky * 0.05), this));
				} else if (color == klic) {
					if (level3.ulozeneKlice == null) {
						klice.add(new Klic(x * velikostBunky, y * velikostBunky, velikostBunky, velikostBunky, this));
					}
				} else if (color == dvere) {
					dvereList.add(new Dvere(x * velikostBunky, y * velikostBunky, velikostBunky * 2, velikostBunky * 2,
							this, 5));
				} else if (color == slimak) {
					slimaci.add(new Slimak(x * velikostBunky, y * velikostBunky, velikostBunky, velikostBunky, this));
				}
			}

		}
		if (!(level3.ulozeneKlice == null)) {
			// System.out.println("nacteni");
			klice = new ArrayList<>();
			klice.addAll(level3.ulozeneKlice);
		}

	}

	@SuppressWarnings("unused")
	public void paint(Graphics g) {

		super.paint(g);

		Graphics2D g2d = (Graphics2D) g;
		if (stenyNaObrazovce != null) {
			for (Stena stena : stenyNaObrazovce) {
				stena.draw(g2d, obrazovka);
			}
		}
		if (bodaky != null) {
			for (Bodak bodak : bodaky) {
				bodak.draw(g2d, obrazovka);
			}
		}
		if (zebriky != null) {
			for (Zebrik zebrik : zebriky) {
				zebrik.draw(g2d, obrazovka);
			}
		}
		if (zebriky != null) {
			for (Checkpoint checkpoint : checkpointy) {
				checkpoint.draw(g2d, obrazovka);
			}
		}
		if (klice != null) {
			for (Klic klic : klice) {
				klic.draw(g2d, obrazovka);
			}
		}
		if (dvereList != null) {
			for (Dvere dvere : dvereList) {
				dvere.draw(g2d, obrazovka);
			}
		}
		if (veze != null) {
			for (Vez vez : veze) {
				vez.draw(g2d, obrazovka);
				if (Test.lajnyVeze == true) {
					g2d.setColor(Color.red);
					g2d.drawLine(player.pozice.x + player.velikost.x / 2, player.pozice.y + player.velikost.y / 2,
							vez.pozice.x + velikostBunky - this.posun.x, vez.pozice.y + velikostBunky);
				}
			}
		}
		if (veze != null) {
			for (Slimak slimak : slimaci) {
				slimak.draw(g2d, obrazovka);
				if (Test.lajnyVeze == true) {
					g2d.setColor(Color.red);
					g2d.drawLine(player.pozice.x + player.velikost.x / 2, player.pozice.y + player.velikost.y / 2,
							slimak.pozice.x + velikostBunky - this.posun.x, slimak.pozice.y + velikostBunky);
				}
			}
		}
		player.draw(g2d);
		// strely
		for (Strela strela : strelyHrace) {
			strela.draw(g2d, obrazovka);
		}
		for (Strela strela : strelyEntit) {
			strela.draw(g2d, obrazovka);
		}
		if (Test.hitBoxObrazovka == true) {
			g2d.setColor(Color.red);
			g2d.draw(obrazovka);
		}
	}

	public void keyReleased(KeyEvent e) {
		switch (Character.toLowerCase(e.getKeyChar())) {
		case 'w':
			player.up = false; break;
		case 'a':
			player.left = false; break;
		case 's':
			player.down = false; break;
		case 'd':
			player.right = false; break;
		case 'r':
			this.restart(); break;
		case 't':
			this.konec();
		case KeyEvent.VK_SPACE:
			player.strilet = false; break;
		}
	}

	public void konec() {
		bodaky = null;
		synchronized (Level3.t) {
			try {
				if (skoncit == false) {
					skoncit = true;
					// System.out.println("KONEC");
					Level3.t.notify();
				}
			} catch (Exception e) {
				System.out.println("CHYBA  -- KONEC " + Thread.currentThread());
			}

		}
	}

	public void restart() {
		// System.out.println("RESTART");
		// restartovani klicu
		level3.ulozeneKlice = new ArrayList<>();
		level3.ulozeneKlice.addAll(klice);

		level3.konec = false;
		konec();
	}

	public void keyPressed(KeyEvent e) {
		switch (Character.toLowerCase(e.getKeyChar())) {
		case 'w':
			player.up = true; break;
		case 'a':
			player.left = true; break;
		case 's':
			player.down = true; break;
		case 'd':
			player.right = true; break;
		case KeyEvent.VK_SPACE:
			player.strilet = true; break;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}

package Levely.Level_3;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class PohybVstup extends KeyAdapter{
	Panel_level3 panel;
	public PohybVstup(Panel_level3 panel) {
		
		this.panel = panel;
	}
	@Override
	public void keyPressed(KeyEvent e) {
		panel.keyPressed(e);
	}
	@Override
	public void keyReleased(KeyEvent e) {
		panel.keyReleased(e);
	}
}

package Levely.Level_1;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import java.util.Random;

import javax.swing.*;
import javax.swing.border.LineBorder;

import main.NesnupejteDrozdi;
import main.Okno;
import main.RelatvniVelikost;

public class Level1 implements ActionListener {
	ImageIcon drozdi = Okno.resizeImage(new ImageIcon(getClass().getClassLoader().getResource("drozdi.png")), 200, 200);
	JButton tlacitko;
	JLabel tlacitkoLabel;
	int nasnupano = 0;
	Okno mistniOkno;
	int stav = 0; // urcuje co maji delat tlacitka pri zmacknuti
	String a = null;
	boolean t1open = true;
	boolean t2open = true;
	String otazka = "Našel jsi na zemi droždí, co s tím ?";
	boolean dalsilevel = false; // nevim proc, ale aby se neotevreli 2 okna
	public JButton tlacitko1;
	public JButton tlacitko2;
	FileManager filemanager;
public Level1(Okno okno) {
		mistniOkno = okno;
		
		okno.smazat();
		this.zaklad(okno);
		okno.nastavOtazku(okno, okno.odpovediPanel, okno.otazkyLabel, tlacitko1, tlacitko2, otazka,
				"Vyhodit droždí do koše", "Vyšňupat droždí");


		// tlacitka nasteveni chovani
		tlacitko2.addChangeListener(e -> {
			JButton b = (JButton) e.getSource();
			ButtonModel m = b.getModel();
			boolean isPressedAndArmed = m.isPressed() && m.isArmed();
			if (t1open == true) {
				a = mistniOkno.otazkyLabel.getText();
			}
			if (isPressedAndArmed) {
				if (stav == 0 || stav == 1) {
					mistniOkno.otazkyLabel.setText("Na tlačítko nešahej a seber to droždí !");
					if (stav == 0) {
						stav = 1; // hrac muze klikat na drozdi
					}
				}
				if (stav == 2) {
					synchronized (this) {
						mistniOkno.smazat();
						notify();
					}
				}
				t1open = false;
			} else {
				mistniOkno.otazkyLabel.setText(a);
				t1open = true;
			}
		});
		tlacitko1.addChangeListener(e -> {
			JButton b = (JButton) e.getSource();
			ButtonModel m = b.getModel();
			boolean isPressedAndArmed = m.isPressed() && m.isArmed();
			if (t2open == true) {
				a = mistniOkno.otazkyLabel.getText();
			}
			if (isPressedAndArmed) {
				if (stav == 0 || stav == 1) {
					mistniOkno.otazkyLabel.setText("Bohužel, koš tu nevidím");
				}
				if (stav == 2) {
					System.out.println(stav + " tl 1");
					synchronized (this) {
						notify();
					}
				}

				t2open = false;
			} else {
				mistniOkno.otazkyLabel.setText(a);
				t2open = true;
			}
		});
		okno.hlPanel.add(tlacitko);
		// prekresleni (pridani grafick7ch zmen)
		okno.repaint();
		// cekani dokud nezmackne konecna tlacitko
		long cas = System.currentTimeMillis(); 
		synchronized (this) {
			try {
				this.wait();
			} catch (InterruptedException ex) {
				System.out.println("CHYBA -- Level1");
			}
		}
		//kontrola nejlepsiho casu
		cas = System.currentTimeMillis() - cas;
		if (NesnupejteDrozdi.casLevel1 > cas) {
			NesnupejteDrozdi.casLevel1 = cas;
		}
		System.out.println("Level1 čas: " + cas);
		okno.odpovediPanel.remove(tlacitko1);
		okno.odpovediPanel.remove(tlacitko2);
		okno.hlPanel.remove(tlacitko);
		// okno.repaint();
		System.out.println("Level1 -- KONEC");

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// System.out.println("Tlacitko");
		if (stav != 0) {
			if (nasnupano >= 100) {
				mistniOkno.nastavOtazku(mistniOkno, mistniOkno.odpovediPanel, mistniOkno.otazkyLabel, tlacitko1,
						tlacitko2, "Našňupáno prostě hodně droždí !! ", "Jít dál", "Jít dál");
				stav = 2; // aby se zmenily listenery tlacitka na konecnou funkci

			} else {
				nasnupano = nasnupano + new Random().nextInt(7);
				mistniOkno.otazkyLabel.setText("Našňupáno " + nasnupano + "% droždí !!");
			}
		}

	}

	@SuppressWarnings("serial")
	void zaklad(Okno okno) {
		okno.defOkno();
		filemanager = new FileManager();
		tlacitko1 = new JButton();
		tlacitko2 = new JButton();
		tlacitko = new JButton();

		okno.otazkyLabel.setLayout(null);
		okno.otazkyLabel.setFont(new Font("Consolas", Font.PLAIN, 35));
		okno.otazkyLabel.setForeground(Color.white);
		okno.otazkyLabel.setOpaque(false);
		okno.otazkyLabel.setBorder(new LineBorder(Color.gray, 30));
		okno.otazkyLabel.setVerticalAlignment(JLabel.TOP);
		
		okno.remove(okno.hlPanel);
		
		okno.hlPanel = new JPanel(){
			
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D)g;
				g2d.fillRect(0,0,100,100);
				g2d.drawImage(FileManager.cesta, 0, 0, this.getSize().width,this.getSize().height, null);
			}
		};
		okno.add(okno.hlPanel);
		okno.hlPanel.setLayout(null);
		okno.hlPanel.setBounds(RelatvniVelikost.obdelnik(20,0,60,75));
		okno.hlPanel.setOpaque(false);
		
		okno.otazkyPanel.setBounds(RelatvniVelikost.obdelnik(0,75,100,10));
		okno.otazkyPanel.setBackground(Color.gray);
		okno.otazkyPanel.setVisible(true);
		
		okno.odpovediPanel.setLayout(null);
		okno.odpovediPanel.setBounds(RelatvniVelikost.obdelnik(0,85,100,15));
		okno.odpovediPanel.setBackground(Color.gray);
		okno.odpovediPanel.setVisible(true);
		okno.odpovediPanel.setOpaque(false);

		// drozdi talcitko
		tlacitko.setBounds(okno.hlPanel.getWidth() / 2 - 100, okno.hlPanel.getHeight() / 2 - 100, 200, 200);
		tlacitko.setIcon(drozdi);
		tlacitko.addActionListener(this);
		tlacitko.setOpaque(false);
		tlacitko.setContentAreaFilled(false);
		tlacitko.setFocusable(false);
		tlacitko.setBorder(null);
		tlacitko.setVisible(true);
		okno.hlPanel.add(tlacitko);
		
		//reklamni panely
		JLabel reklamniPannel1 =  new JLabel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D)g;
				g2d.drawImage(FileManager.reklamnihoBanner, 0, 0, this.getSize().width,this.getSize().height, null);
			}
		};
		reklamniPannel1.setOpaque(false);
		reklamniPannel1.setBounds(RelatvniVelikost.obdelnik(0,0,20,75));
		okno.add(reklamniPannel1);
		
		JLabel reklamniPannel2 =  new JLabel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D)g;
				g2d.drawImage(FileManager.reklamnihoBanner, 0, 0, this.getSize().width,this.getSize().height, null);
			}
		};
		reklamniPannel2.setOpaque(false);
		reklamniPannel2.setBounds(RelatvniVelikost.obdelnik(80,0,20,75));
		okno.add(reklamniPannel2);
	}
}

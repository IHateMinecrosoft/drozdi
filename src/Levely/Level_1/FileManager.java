package Levely.Level_1;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import main.Okno;

public class FileManager {
	public static BufferedImage cesta, reklamnihoBanner;
	public FileManager() {
		try {
			cesta = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level1/cesta.jpg")));
		} catch (Exception e) {
			System.out.println("CHYBA -- Level1 -- Nacteni obrazku cesty " + e);
		}
		try {
			reklamnihoBanner = ImageIO.read(new File(Okno.ziskatCestu("rsc/Level1/reklamniBanner1.png")));
		} catch (Exception e) {
			System.out.println("CHYBA -- Level1 -- Nacteni obrazku reklamnihoBanneru1 " + e);
		}
	}
}
